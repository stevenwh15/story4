from .views import get_activities, activities_detail, activities_peserta_delete
from django.urls import include, path

urlpatterns = [
    path('activities/<str:name>', activities_detail, name='activities-detail'),
    path('activities/<str:nama>/delete', activities_peserta_delete, name='peserta-delete'),
    path('', get_activities, name='get-activities')
]