from django import forms
from story6.models import Kegiatan, Pendaftar

class tambah_kegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'

class tambah_peserta(forms.ModelForm):
    class Meta:
        model = Pendaftar
        fields = '__all__'
        widgets = {
            'acara': forms.HiddenInput()
        }
    