from django.db import models

class Kegiatan(models.Model):
    nama = models.CharField(max_length=20, verbose_name="Kegiatan", primary_key=True)
    deskripsi = models.TextField(blank=True)

class Pendaftar(models.Model):
    nama = models.CharField(max_length=30)
    acara = models.ForeignKey(Kegiatan, on_delete= models.CASCADE, related_name="pendaftar")