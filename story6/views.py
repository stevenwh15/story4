from django.shortcuts import render, get_object_or_404
from .models import Kegiatan, Pendaftar
from django.views.generic import TemplateView
from .form import tambah_kegiatan, tambah_peserta

def get_activities(req):
    if req.method == 'POST':
        form_kegiatan = tambah_kegiatan(req.POST)
        if form_kegiatan.is_valid():
            form_kegiatan.save()
        activities = Kegiatan.objects.all()
        form_kegiatan = tambah_kegiatan()
        return render(req, "story6/activities.html", {'activities': activities, 'form_kegiatan': form_kegiatan })
    else:
        activities = Kegiatan.objects.all()
        kegiatan_form = tambah_kegiatan()
        return render(req, "story6/activities.html", {'activities': activities, 'form_kegiatan': kegiatan_form })

def activities_peserta_delete(req, nama):
    if req.method == 'POST':
        try:
            id_peserta = req.POST.get('id')
            Pendaftar.objects.get(pk=id_peserta).delete()
        except:
            kegiatan_req = get_object_or_404(Kegiatan, pk=nama)
            form_peserta = tambah_peserta(initial={'acara': kegiatan_req})
            return render(req, 'story6/peserta.html', {'kegiatan': kegiatan_req, 'form_peserta': form_peserta})
    kegiatan_req = get_object_or_404(Kegiatan, pk=nama)
    form_peserta = tambah_peserta(initial={'acara': kegiatan_req})
    return render(req, 'story6/peserta.html', {'kegiatan': kegiatan_req, 'form_peserta': form_peserta})

def activities_detail(req, name):
    if req.method == 'POST':
        form_peserta = tambah_peserta(req.POST)
        if form_peserta.is_valid():
            form_peserta.save()
            kegiatan_req = get_object_or_404(Kegiatan, pk=name)
            form_peserta = tambah_peserta(initial={'acara': kegiatan_req})
        kegiatan_req = get_object_or_404(Kegiatan, pk=name)
        return render(req, 'story6/peserta.html', {'kegiatan': kegiatan_req, 'form_peserta': form_peserta})

    else:
        kegiatan_req = get_object_or_404(Kegiatan, pk=name)
        form_peserta = tambah_peserta(initial={'acara': kegiatan_req})
        return render(req, 'story6/peserta.html', {'kegiatan': kegiatan_req, 'form_peserta': form_peserta})