from django.contrib import admin
from .models import Kegiatan, Pendaftar

admin.site.register(Kegiatan)
admin.site.register(Pendaftar)