from django.test import TestCase, override_settings
from django.test import Client
from django.urls import resolve, reverse
from .views import get_activities, activities_detail
from .models import Kegiatan, Pendaftar


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class kegiatanUnitTest(TestCase):
    def setUp(self):
        self.test_kegiatan = Kegiatan.objects.create(nama='Test', deskripsi='Holaha')

    def test_kegiatan_all_url_is_exist(self):
        response = Client().get('/activities/')
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_all_using_view_func(self):
        found = resolve('/activities/')
        self.assertEqual(found.func, get_activities)

    def test_kegiatan_detail_url_is_exist(self):
        response = Client().get('/activities/activities/Test')
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_detail_using_view_func(self):
        found = resolve('/activities/activities/Test')
        self.assertEqual(found.func, activities_detail)

    def test_kegiatan_add_post_success_and_render_the_result(self):
        test = 'Haha'
        response_post = Client().post(
            '/activities/', {'nama': 'Haha', 'deskripsi': 'Hola'})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_kegiatan_pendaftar_delete_post_success_and_render_the_result(self):
        test = 'Bla'
        pd = Pendaftar.objects.create(nama='Bla', acara=self.test_kegiatan)
        response_post = Client().post(
            '/activities/activities/Test/delete', {'id': pd.id})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_kegiatan_pendaftar_delete_post_fail_and_render_the_result(self):
        test = 'Bla'
        pd = Pendaftar.objects.create(nama='Bla', acara=self.test_kegiatan)
        response_post = Client().post(
            '/activities/activities/Test/delete', {'id': 2})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_pendaftar_add_post_success_and_render_the_result(self):
        response_post = Client().post(
            '/activities/activities/Test', {'nama': 'WH', 'acara': self.test_kegiatan.nama})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertIn('WH', html_response)
