from django.shortcuts import render, HttpResponse

def index(request):
    return render(request, 'story1/index.html')