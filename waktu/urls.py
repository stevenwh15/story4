from django.urls import path

from .views import timeview

urlpatterns = [
    path('', timeview, name="time"),
    path('<int:offset>', timeview)
]
