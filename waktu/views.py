from django.shortcuts import render
import datetime

# Create your views here.
def timeview(req, offset=0):
    t = datetime.datetime.now()
    t = t + datetime.timedelta(hours=offset)
    return render(req, "waktu/time.html", {"t": t, "offset": offset})