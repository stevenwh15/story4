from django import forms
from Story5.models import Post

class Matkul(forms.ModelForm):
    class Meta:
        model = Post
        fields = '__all__'

class DeleteMatkul(forms.Form):
    def __init__(self, matkuls, *args, **kwargs):
        super(DeleteMatkul, self).__init__(*args, **kwargs)
        self.fields["matkuls"].choices = matkuls
    
    matkuls = forms.ChoiceField(choices=(), required=True, label="Nama Matkul")
