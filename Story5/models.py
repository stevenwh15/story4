from django.db import models

class Post(models.Model):
    matkul = models.CharField(max_length=20, verbose_name="Mata Kuliah", primary_key=True)
    dosen = models.CharField(max_length=25)
    kelas = models.CharField(max_length=10, verbose_name="Ruang kelas")
    sks = models.IntegerField(verbose_name="Jumlah SKS")
    semester = models.CharField(max_length=30,)
    deskripsi = models.TextField(blank=True)

class Tugas(models.Model):
    nama = models.CharField(max_length=30)
    matakuliah = models.ForeignKey(Post, on_delete= models.CASCADE, related_name="tugas")
    deskripsi = models.TextField(blank=True)
    deadline = models.DateField()

    class Meta:
        ordering = ["-deadline"]