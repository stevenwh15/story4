from django.shortcuts import render, redirect
from .forms import Matkul, DeleteMatkul
from .models import Post, Tugas
from django.views.generic import TemplateView
import datetime


class pelajaran(TemplateView):
    template_name = "story5/tambah.html"

    def get(self,req):
        matkuls = [(matkul.matkul, matkul.matkul) for matkul in Post.objects.all()]
        form = Matkul()
        form_delete = DeleteMatkul(matkuls)
        return render(req, self.template_name, {"form": form, "delete": form_delete})
    
    def post(self,req):
        form = Matkul(req.POST)
        if form.is_valid():
            form.save()
            return redirect("get-pelajaran")
        return render(req, self.template_name, {"form": form})

class delete_pelajaran(TemplateView):
    template_name = "story5/tambah.html"

    def get(self,req):
        matkuls = [(matkul.matkul, matkul.matkul) for matkul in Post.objects.all()]
        form = Matkul()
        form_delete = DeleteMatkul(matkuls)
        return render(req, self.template_name, {"form": form, "delete": form_delete})
    
    def post(self,req):
        form = Matkul()
        matkuls = [(matkul.matkul, matkul.matkul) for matkul in Post.objects.all()]
        form_delete = DeleteMatkul(matkuls, req.POST)
        if form_delete.is_valid():
            print(form_delete)
            Post.objects.get(matkul=req.POST.get("matkuls")).delete()
            return redirect("get-pelajaran")
        return render(req, self.template_name, {"form": form, "delete": form_delete})
    
def get_pelajaran(req):
    pelajaran = Post.objects.all()
    return render(req, "story5/jadwal.html", {'pelajaran': pelajaran})

def view_matkul(req, name):
    matkul = Post.objects.get(pk=name)
    tugas = matkul.tugas.all()
    currtime = datetime.datetime.now()
    tugas_lewat = [t for t in tugas if t.deadline < currtime.date()]
    tugas_deket = [t for t in tugas if (t.deadline - currtime.date()).days <= 2 and t not in tugas_lewat]
    tugas_jauh = [t for t in tugas if (t.deadline - currtime.date()).days > 2 and t not in tugas_lewat and t not in tugas_deket]

    return render(req, "story5/matkul.html", {"matkul": matkul, "tugas_deket": tugas_deket, "tugas_lewat": tugas_lewat, "tugas_jauh": tugas_jauh})