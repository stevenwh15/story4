from django.urls import include, path
from .views import pelajaran, get_pelajaran, delete_pelajaran, view_matkul

urlpatterns = [
    path('tambah', pelajaran.as_view(), name='tambah-pelajaran'),
    path('hapus', delete_pelajaran.as_view(), name='hapus-pelajaran'),
    path('jadwal', get_pelajaran, name='get-pelajaran'),
    path('matkul/<str:name>', view_matkul, name='view-matkul')
]