from django.urls import include, path
from .views import index, about, record,gallery

urlpatterns = [
    path('', index, name='index3'),
    path('about', about, name='about'),
    path('record', record, name='record'),
    path('gallery', gallery, name= 'gallery'),
]
