from django.shortcuts import render

def index(request):
    return render(request, 'story3/index.html')

def about(request):
    return render(request, 'story3/about.html')

def record(request):
    return render(request, 'story3/record.html')

def gallery(request):
    return render(request, 'story3/gallery.html')